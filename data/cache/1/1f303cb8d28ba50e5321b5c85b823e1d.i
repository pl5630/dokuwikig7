a:60:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:99:"dokuwiki 是一套以 php 編寫, 無需資料庫的 wiki 系統, 一般執行 php 程式的方式:";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:101;}i:4;a:3:{i:0;s:10:"listo_open";i:1;a:0:{}i:2;i:101;}i:5;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:101;}i:6;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:101;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:81:" 直接利用 PHP 5.4.0 版之後內建的全球資訊網來解譯 php 程式碼.";}i:2;i:105;}i:8;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:186;}i:9;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:186;}i:10;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:186;}i:11;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:186;}i:12;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:124:" 利用 web server 附加的延伸功能, 解譯 php 程式碼, 例如: Apache, nginx 或 mongoose 全球資訊網伺服器.";}i:2;i:190;}i:13;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:314;}i:14;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:314;}i:15;a:3:{i:0;s:11:"listo_close";i:1;a:0:{}i:2;i:314;}i:16;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:27:"以PHP內建伺服器執行";i:1;i:3;i:2;i:316;}i:2;i:316;}i:17;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:316;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:316;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"執行步驟";}i:2;i:354;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:366;}i:21;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:263:"Step 1:下載 Windows 版本的 php 解譯系統
Step 2:將系統搜尋路徑指項 php.exe 所對應的目錄
Step 3:將 router.php 程式檔放入 dokuwiki index.php 所在目錄
Step 4:啟動 dos command, 執行 php -S localhost:8000 -t D:\dokuwiki router.php
";}i:2;i:366;}i:22;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:366;}i:23;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:61:"http://www.php.net/manual/en/features.commandline.options.php";i:1;s:20:"php.exe 選項設定";}i:2;i:641;}i:24;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:727;}i:25;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:727;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:" -t 用來宣告 document root ";}i:2;i:731;}i:27;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:762;}i:28;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:82:"
 啟動後, 利用流覽器至 localhost:8000 即可看到 dokuwiki 執行結果.";}i:2;i:764;}i:29;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:846;}i:30;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:484:"
<?php
// router.php
 
$root = $_SERVER['DOCUMENT_ROOT'];
chdir($root);
$path = '/'.ltrim(parse_url($_SERVER['REQUEST_URI'])['path'],'/');
set_include_path(get_include_path().':'.__DIR__);
if(file_exists($root.$path))
{
	if(is_dir($root.$path) && substr($path,strlen($path) - 1, 1) !== '/')
		$path = rtrim($path,'/').'/index.php';
	if(strpos($path,'.php') === false) return false;
	else {
		chdir(dirname($root.$path));
		require_once $root.$path;
	}
}else include_once 'index.php';
";i:1;s:3:"php";i:2;s:10:"router.php";}i:2;i:853;}i:31;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1362;}i:32;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:30:"以 mongoose web server 執行";i:1;i:3;i:2;i:1362;}i:2;i:1362;}i:33;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:1362;}i:34;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1362;}i:35;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:29:"https://copy.com/So6e4kSQ9vuc";i:1;s:26:"下載 mongoose web server";}i:2;i:1403;}i:36;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1463;}i:37;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1463;}i:38;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"mongoose.conf
 ";}i:2;i:1466;}i:39;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1486;}i:40;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:1886:"
# Mongoose web server configuration file.
# Lines starting with '#' and empty lines are ignored.
# For detailed description of every option, visit
# http://code.google.com/p/mongoose/wiki/MongooseManual
#
# cgi_pattern **.cgi$|**.pl$|**.php$|**.py$
# 只將 php 檔案當作 cgi 可執行程式
cgi_pattern **.php$
# cgi_environment <value>
# put_delete_passwords_file <value>
# 直接設定利用 php 解譯 php
cgi_interpreter V:\IDE\php-5.5.9-nts-Win32-VC11-x86\php-cgi.exe -c V:\IDE\php-5.5.9-nts-Win32-VC11-x86\php.ini
#
# 用 protect_uri 來保護特定目錄
#protect_uri /content=V:/certificates/password.txt,/cmsimple=V:/certificates/password.txt
#
# 設定保護回應的 domain
# authentication_domain mydomain.com
# ssi_pattern **.shtml$|**.shtm$
# throttle <value>
access_log_file V:/tmp/mongoose_access_log.txt
#
# 關閉目錄資料列出功能
enable_directory_listing no
error_log_file V:/tmp/mongoose_error_log.txt
# global_passwords_file <value>
# index_files index.html,index.htm,index.cgi,index.shtml,index.php
# enable_keep_alive no
# access_control_list <value>
# extra_mime_types <value>
#
listening_ports 8084,443s
document_root V:/www/dokuwiki
#
# SSL 數位簽署檔案
ssl_certificate V:/certificates/ssl_cert.pem
#
# num_threads 20
# run_as_user <value>
# url_rewrite_patterns <value>
# hide_files_patterns <value>
#
# access control list (ACL), - for deny and + for allow
# 只允許 127.0.0.1 連結
l	-0.0.0.0/0,+127.0.0.1,+192.168.1.0/24
#
# alias mapping support, 將 local 的 V:\temp 對應為 http://localhost/temp, 多重對應以逗點隔開
# w /temp=V:\temp,/tmp=V:\tmp
#w /exam=V:\TCExam,/phpmyadmin=V:\phpMyAdmin-3.5.3-english,/cms=V:\cmsimplexh1542
#w /dnd=V:\dndUploader-master\public_html,/three.js=V:\three.js,/brython=V:\Brython-20121229-085018
# hide file pattern, 讓使用者無法連結 所有 .htm 與 .db 檔案
# x **.htm$,**.db$
";i:1;s:14:"moongoose.conf";i:2;N;}i:2;i:1486;}i:41;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3397;}i:42;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:15:"安裝 Dokuwiki";i:1;i:3;i:2;i:3397;}i:2;i:3397;}i:43;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:3397;}i:44;a:3:{i:0;s:10:"listo_open";i:1;a:0:{}i:2;i:3423;}i:45;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:3423;}i:46;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:3423;}i:47;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:" 執行 ";}i:2;i:3427;}i:48;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:33:"http://localhost:8000/install.php";i:1;N;}i:2;i:3435;}i:49;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:" 安裝";}i:2;i:3468;}i:50;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:3475;}i:51;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:3475;}i:52;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:3475;}i:53;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:3475;}i:54;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:163:" Dokuwiki 安裝之後, 會產生 conf/local.php, users.auth.php 與 acl.auth.php 等三個檔案, 完成安裝後若要重新安裝則需要刪除此三個檔案.";}i:2;i:3479;}i:55;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:3642;}i:56;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:3642;}i:57;a:3:{i:0;s:11:"listo_close";i:1;a:0:{}i:2;i:3642;}i:58;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3642;}i:59;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:3642;}}